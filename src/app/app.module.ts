import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AngularFireModule } from 'angularfire2';
import { LoginPageComponent } from './login-page/login-page.component';
import { RouterModule, Routes } from "@angular/router";
import { AF } from "../providers/af";
import { HomePageComponent } from './home-page/home-page.component';
import { FormsModule } from "@angular/forms";

export const firebaseConfig = {
    apiKey: "AIzaSyDyJcGiUukJV333b5RJdEHjFnsdahppfUI",
    authDomain: "test-app-f02cb.firebaseapp.com",
    databaseURL: "https://test-app-f02cb.firebaseio.com",
    projectId: "test-app-f02cb",
    storageBucket: "test-app-f02cb.appspot.com",
    messagingSenderId: "620769407080"
};

const routes: Routes = [
    { path: 'home', component: HomePageComponent },
    { path: 'login', component: LoginPageComponent }
];

@NgModule({
    imports: [
        BrowserModule,
        AngularFireModule.initializeApp(firebaseConfig),
        RouterModule.forRoot(routes),
        FormsModule
    ],
    declarations: [ AppComponent, LoginPageComponent, HomePageComponent ],
    bootstrap: [ AppComponent ],
    providers: [ AF ]
})
export class AppModule {}
