import { Component } from '@angular/core';
import { Router } from "@angular/router";

import { AF } from ".././../providers/af";

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.css']
})

export class LoginPageComponent {
    public error: any;

    constructor(public appService: AF, private router: Router) {}

    loginWithGoogle() {
        this.appService.loginWithGoogle().then((data) => {
            this.appService.addUserInfo();
            this.router.navigate(['home']);
        })
    }
}
