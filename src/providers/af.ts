import {Injectable} from "@angular/core";
import {AngularFire, AuthProviders, AuthMethods, FirebaseListObservable} from 'angularfire2';

@Injectable()
export class AF {
    public messages: FirebaseListObservable<any>;
    public users: FirebaseListObservable<any>;
    public displayName: string;
    public email: string;

    constructor(public af: AngularFire) {
        this.messages = this.af.database.list('messages');
        this.users = this.af.database.list('users');
    }

    loginWithGoogle() {
        return this.af.auth.login({
            provider: AuthProviders.Google,
            method: AuthMethods.Popup,
        });
    }

    logout() {
        return this.af.auth.logout();
    }

    addUserInfo(){
        this.users.push({
            email: this.email,
            displayName: this.displayName
        });
    }

    sendMessage(text) {
        var message = {
            message: text,
            displayName: this.displayName,
            email: this.email,
            timestamp: Date.now()
        };
        this.messages.push(message);
    }
}
